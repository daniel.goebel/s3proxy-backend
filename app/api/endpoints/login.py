import urllib
import urllib.parse
from typing import Annotated

from fastapi import APIRouter, BackgroundTasks, Depends, Query, Request, status
from fastapi.responses import RedirectResponse
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema

from app.api.background.email import send_first_login_email
from app.api.dependencies import DBSession, LoginException, RGWService, verify_and_fetch_userinfo
from app.core import security
from app.core.config import settings
from app.crud import CRUDUser
from app.models import User
from app.otlp import start_as_current_span_async
from app.schemas.security import UserInfo

router = APIRouter(prefix="/auth", tags=["Auth"])
tracer = trace.get_tracer_provider().get_tracer(__name__)

RETURN_PATH_KEY = "return_path"


def build_url(base_url: str, *path: str) -> str:
    # Returns a list in the structure of urlparse.ParseResult
    url_parts = list(urllib.parse.urlparse(base_url))
    url_parts[2] = "/".join(path)
    return urllib.parse.urlunparse(url_parts)


@router.get(
    "/login",
    status_code=status.HTTP_302_FOUND,
    summary="Redirect to LifeScience OIDC Login",
)
@start_as_current_span_async("api_login_route", tracer=tracer)
async def login(
    request: Request,
    provider: Annotated[
        security.OIDCProvider, Query(description="The OIDC provider to use for login")
    ] = security.OIDCProvider.lifescience,
    return_path: Annotated[
        str | SkipJsonSchema[None],
        Query(
            max_length=128,
            description="Will be appended to redirect response in the callback route as URL query parameter `return_path`",
        ),
    ] = None,
) -> RedirectResponse:
    """
    Redirect route to OIDC provider to kickstart the login process.
    \f
    Parameters
    ----------
    provider : app.core.security.OIDCProvider
        Query parameter to indicate the OIDC provider.
    request : fastapi.requests.Request
        Raw request object.
    return_path : str | None
        Query parameter that gets stored in the session cookie.
        Will be appended to RedirectResponse in the callback route as URL query parameter 'return_path'

    Returns
    -------
    response : fastapi.responses.RedirectResponse
        Redirect response to right OAuth2 endpoint
    """
    # Clear session to prevent an overflow
    request.session.clear()
    print(request)
    if return_path:
        request.session[RETURN_PATH_KEY] = return_path
    redirect_uri = build_url(str(settings.ui_uri), settings.api_prefix, router.prefix[1:], "callback", provider.name)
    return await security.get_provider(provider).authorize_redirect(request, redirect_uri=redirect_uri)


@router.get(
    "/callback/{provider}",
    response_class=RedirectResponse,
    status_code=status.HTTP_302_FOUND,
    summary="LifeScience Login Callback",
    responses={
        status.HTTP_302_FOUND: {
            "headers": {
                "Set-Cookie": {
                    "description": "JWT for accessing the API",
                    "schema": {
                        "type": "string",
                        "example": "bearer=fake-jwt-cookie; Domain=localhost; expired=Wed, 05 Jan 2022 "
                        "09:00:00 GMT; Path=/; SameSite=strict; Secure",
                    },
                }
            }
        }
    },
)
@start_as_current_span_async("api_oidc_callback", tracer=tracer)
async def login_callback(
    response: RedirectResponse,
    db: DBSession,
    rgw: RGWService,
    request: Request,
    user_info: Annotated[UserInfo, Depends(verify_and_fetch_userinfo)],
    background_tasks: BackgroundTasks,
) -> str:
    """
    Callback for the Life Science Identity Provider.\n
    Visit the route login route to start the login process.

    If the user is already known to the system, then a JWT token will be created and sent via the 'set-cookie' header.
    The key for this Cookie is 'bearer'.\n
    If the user is new, he will be created and then a JWT token is issued.\n
    This JWT has to be sent to all authorized endpoints via the HTTPBearer scheme.
    \f
    Parameters
    ----------
    response : fastapi.responses.RedirectResponse
        Response which will hold the JWT cookie.
    request : fastapi.Request
        Raw request object to access the session cookie.
    user_info : dict[str, Any]
        Get the userinfo with OAuth2. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    user_info: app.schemas.security.UserInfo
        Info about the user fetched from the user info endpoint

    Returns
    -------
    path : str
        Redirect path after successful login.
    """
    redirect_path = "/"
    current_span = trace.get_current_span()
    return_path: str | None = request.session.get(RETURN_PATH_KEY, None)  # get return path from session cookie
    if return_path is not None:
        current_span.set_attribute("return_path", return_path)
        redirect_path += f"?return_path={urllib.parse.quote_plus(return_path)}"
    request.session.clear()
    try:
        lifescience_id = user_info.sub if isinstance(user_info.sub, str) else user_info.sub[0]
        current_span.set_attribute("lifescience_id", lifescience_id)
        lifescience_id = lifescience_id.split("@")[0]

        user = await CRUDUser.get_by_lifescience_id(lifescience_id=lifescience_id, db=db)
        if user is None:
            if settings.block_foreign_users:  # pragma: no cover
                raise LoginException(error_source="Access Denied")
            user = await CRUDUser.create(
                User(lifescience_id=lifescience_id, display_name=user_info.name, email=user_info.email),
                db=db,
            )
            with tracer.start_as_current_span(
                "rgw_create_user", attributes={"uid": str(user.uid), "display_name": user.display_name}
            ):
                rgw.create_user(
                    uid=str(user.uid),
                    max_buckets=-1,
                    display_name=user.display_name,
                )
            background_tasks.add_task(send_first_login_email, user=user)

        elif user.email != user_info.email:
            await CRUDUser.update_email(user.uid, user_info.email, db=db)

        jwt = security.create_access_token(str(user.uid))
        response.set_cookie(
            key="bearer",
            value=jwt,
            samesite="strict",
            max_age=settings.jwt_token_expire_minutes * 60,
            secure=True,
            domain=settings.ui_uri.host,
        )
    except LoginException as e:  # pragma: no cover
        raise e
    except Exception:  # pragma: no cover
        raise LoginException(error_source="server")
    return redirect_path
