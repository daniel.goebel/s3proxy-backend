from typing import Sequence
from uuid import UUID

from fastapi import APIRouter, HTTPException, Path, Query, status
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema
from typing_extensions import Annotated

from app.api.dependencies import CurrentUser, DBSession
from app.crud import CRUDUser
from app.models import User
from app.otlp import start_as_current_span_async
from app.schemas.user import UserOut

router = APIRouter(prefix="/users", tags=["User"])

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("/me", response_model=UserOut, summary="Get the logged in user")
@start_as_current_span_async("api_get_logged_in_user", tracer=tracer)
async def get_logged_in_user(current_user: CurrentUser) -> User:
    """
    Return the user associated with the used JWT.
    \f
    Parameters
    ----------
    current_user : app.models.User
        User from the database associated to the used JWT. Dependency injection.

    Returns
    -------
    current_user : app.models.User
        User associated to used JWT.
    """
    return current_user


@router.get("", response_model=list[UserOut], summary="List users and search by their name")
@start_as_current_span_async("api_list_users", tracer=tracer)
async def list_users(
    db: DBSession,
    name_substring: Annotated[
        str | SkipJsonSchema[None],
        Query(
            min_length=3,
            max_length=30,
            description="Filter users by a substring in their name.",
        ),
    ] = None,
) -> Sequence[User]:
    """
    Return the users that have a specific substring in their name.

    Permission `user:read_any` required, except when `name_substring` as only query parameter is set,
    then permission `user:search` required.
    \f
    Parameters
    ----------
    name_substring : string | None, default None
        Filter users by a substring in their name. Query Parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    users: list[app.models.User]
        Users who have the substring in their name.
    """
    current_span = trace.get_current_span()
    if name_substring is not None:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)

    return await CRUDUser.list_users(name_substring=name_substring, db=db)


@router.get("/{uid}", response_model=UserOut, summary="Get a user by its uid")
@start_as_current_span_async("api_get_user", tracer=tracer)
async def get_user(
    db: DBSession,
    uid: Annotated[
        UUID,
        Path(description="UID of a user", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]),
    ],
) -> User:
    """
    Return the user with the specific uid.

    Permission `user:read` required if the current user has the same uid as `uid` otherwise `user:read_any` required.
    \f
    Parameters
    ----------
    uid : uuid.UUID
        The uid of a user. URL path parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : app.models.User
        User with given uid.
    """
    trace.get_current_span().set_attribute("uid", str(uid))
    user = await CRUDUser.get(uid, db=db)
    if user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"User with id '{str(uid)}' not found")
    return user
