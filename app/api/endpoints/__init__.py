from .bucket_permissions import router as bucket_permission_router
from .buckets import router as bucket_router
from .login import router as login_router
from .miscellaneous_endpoints import router as miscellaneous_router
from .s3key import router as s3key_router
from .users import router as users_router

__all__ = [
    "bucket_router",
    "bucket_permission_router",
    "s3key_router",
    "miscellaneous_router",
    "login_router",
    "users_router",
]
