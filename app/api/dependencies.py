from typing import TYPE_CHECKING, Annotated, AsyncGenerator
from uuid import UUID

from authlib.integrations.base_client.errors import OAuthError
from authlib.jose.errors import BadSignatureError, DecodeError, ExpiredTokenError
from fastapi import Depends, HTTPException, Path, Request, status
from fastapi.security import HTTPBearer
from fastapi.security.http import HTTPAuthorizationCredentials
from opentelemetry import trace
from rgwadmin import RGWAdmin
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

from app.ceph.rgw import rgw
from app.ceph.s3 import s3_resource
from app.core import security
from app.core.config import settings
from app.crud.crud_bucket import CRUDBucket
from app.crud.crud_user import CRUDUser
from app.models import Bucket, User
from app.otlp import start_as_current_span_async
from app.schemas.security import JWT, UserInfo

if TYPE_CHECKING:
    from mypy_boto3_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

tracer = trace.get_tracer_provider().get_tracer(__name__)
bearer_token = HTTPBearer(description="JWT Header", auto_error=False)


class LoginException(Exception):
    def __init__(self, error_source: str):
        self.error_source = error_source


def get_rgw_admin() -> RGWAdmin:  # pragma: no cover
    return rgw


RGWService = Annotated[RGWAdmin, Depends(get_rgw_admin)]


def get_s3_resource() -> S3ServiceResource:
    return s3_resource  # pragma: no cover


S3Resource = Annotated[S3ServiceResource, Depends(get_s3_resource)]


async def get_db() -> AsyncGenerator[AsyncSession, None]:  # pragma: no cover
    """
    Get a Session with the database.

    FastAPI Dependency Injection Function.

    Returns
    -------
    db : AsyncGenerator[AsyncSession, None]
        Async session object with the database
    """
    engine_async = create_async_engine(str(settings.db.dsn_async), echo=settings.db.verbose, pool_recycle=3600)
    async with async_sessionmaker(engine_async, expire_on_commit=False)() as session:
        yield session
    await engine_async.dispose()


DBSession = Annotated[AsyncSession, Depends(get_db)]


@start_as_current_span_async("decode_jwt", tracer=tracer)
async def decode_bearer_token(
    token: Annotated[HTTPAuthorizationCredentials | None, Depends(bearer_token)],
    db: DBSession,
) -> JWT:
    """
    Get the decoded JWT or reject request if it is not valid.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : fastapi.security.http.HTTPAuthorizationCredentials
        Bearer token sent with the HTTP request. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    """
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")
    try:
        jwt = JWT(**security.decode_token(token.credentials), raw_token=token.credentials)
        trace.get_current_span().set_attributes({"exp": jwt.exp.isoformat(), "uid": jwt.sub})
        await get_current_user(jwt, db)  # make sure the user exists
        return jwt
    except ExpiredTokenError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="JWT Signature has expired")
    except (DecodeError, BadSignatureError):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")


async def get_current_user(token: Annotated[JWT, Depends(decode_bearer_token)], db: DBSession) -> User:
    """
    Get the current user from the database based on the JWT.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : app.models.User
        User associated with the JWT sent with the HTTP request.
    """
    user = await CRUDUser.get(UUID(token.sub), db=db)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


CurrentUser = Annotated[User, Depends(get_current_user)]


async def get_user_by_path_uid(
    uid: UUID = Path(default=..., description="UID of a user", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]),
    db: AsyncSession = Depends(get_db),
) -> User:
    """
    Get the user from the database with the given uid.
    Reject the request if the current user is not the same as the requested one.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    uid : str
        The uid of a user. URL path parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : app.models.User
        User with the given uid.

    """
    user = await CRUDUser.get(uid, db=db)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


PathUser = Annotated[User, Depends(get_user_by_path_uid)]


async def get_current_bucket(
    bucket_name: Annotated[
        str, Path(..., description="Name of bucket", examples=["test-bucket"], max_length=63, min_length=3)
    ],
    db: DBSession,
) -> Bucket:
    """
    Get the Bucket from the database based on the name in the path.
    Reject the request if user has no permission for this bucket.

    FastAPI Dependency Injection Function

    Parameters
    ----------
    bucket_name : str
        Name of a bucket. URL Path Parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    bucket : app.models.Bucket
        Bucket with the given name.
    """
    bucket = await CRUDBucket.get(bucket_name, db=db)
    if bucket is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Bucket not found")
    return bucket


CurrentBucket = Annotated[Bucket, Depends(get_current_bucket)]


async def verify_and_fetch_userinfo(
    request: Request,
    provider: Annotated[security.OIDCProvider, Path(title="The OIDC provider from which the callback originates")],
) -> UserInfo:  # pragma: no cover
    """
    Authorize the access token from OIDC provider and fetch user information from user endpoint

    Parameters
    ----------
    request : fastapi.Request
        Raw request object
    provider : app.security.OIDCProvider


    Returns
    -------
    userinfo : dict[str, Any]
    """
    try:
        if "error" in request.query_params.keys():
            # if there is an error in the login flow, like a canceled login request, then notify the client
            raise LoginException(error_source=request.query_params["error"])
        with tracer.start_as_current_span("oidc_authorize_access_token"):
            claims = await security.get_provider(provider).authorize_access_token(request)
        # ID token doesn't have all necessary information, call userinfo endpoint
        with tracer.start_as_current_span("oidc_get_user_info"):
            info = await security.get_provider(provider).userinfo(token=claims)
            return UserInfo(sub=info["sub"], name=info["name"], email=info.get("email", None))
    except OAuthError:
        # if there is an error in the oauth flow, like an expired token, then notify the client
        raise LoginException(error_source="oidc")
    except AttributeError:
        raise LoginException(error_source=f"unknown_provider {provider.name}")
