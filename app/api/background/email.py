from email import utils as email_utils
from email.message import EmailMessage
from enum import StrEnum, unique
from typing import Any

from mako.lookup import TemplateLookup
from opentelemetry import trace
from pydantic import NameEmail

from app.api.background.dependencies import smtp_connection
from app.core.config import settings
from app.models import User

tracer = trace.get_tracer_provider().get_tracer(__name__)

_emailHtmlLookup = TemplateLookup(directories=["app/templates/email/html"], module_directory="/tmp/mako_modules")
_emailPlainLookup = TemplateLookup(directories=["app/templates/email/plain"], module_directory="/tmp/mako_modules")


@unique
class EmailTemplates(StrEnum):
    FIRST_LOGIN = "first_login"

    def render(self, **kwargs: Any) -> tuple[str, str]:
        """
        Render both HTML and plain text email templates

        Parameters
        ----------
        kwargs : Any
            Arguments for the email template

        Returns
        -------
        html, plain : tuple[str, str]
            The tuple of the rendered HTML and plain text email
        """
        return _emailHtmlLookup.get_template(f"{self}.html.tmpl").render(
            settings=settings, **kwargs
        ), _emailPlainLookup.get_template(f"{self}.txt.tmpl").render(settings=settings, **kwargs)


def _format_user_to_email(user: User) -> str:
    return f"{user.display_name} <{user.email}>"


def send_email(
    recipients: list[User] | User,
    subject: str,
    plain_msg: str,
    html_msg: str | None = None,
) -> None:
    """
    Send an email.

    Parameters
    ----------
    recipients: list[app.models.User] | app.models.User
        The recipients of the email. Multiple will be sent via BCC
    subject: str
        The subject of the email.
    plain_msg: str
        An alternative message in plain text format.
    html_msg : str
        An alternative message in HTML format.
    """
    email_msg = EmailMessage()
    email_msg["Subject"] = subject
    email_msg["From"] = (
        str(NameEmail(email=settings.smtp.sender_email.email, name="S3Proxy"))
        if settings.smtp.sender_email.email.startswith(settings.smtp.sender_email.name)
        else str(settings.smtp.sender_email)
    )
    if isinstance(recipients, list):
        email_msg["Bcc"] = ",".join(_format_user_to_email(user) for user in recipients)
    else:
        email_msg["To"] = _format_user_to_email(recipients)
    email_msg["Date"] = email_utils.formatdate()
    if settings.smtp.reply_email is not None:  # pragma: no cover
        email_msg["Reply-To"] = (
            f"S3Proxy Support <{settings.smtp.reply_email.email}>"
            if settings.smtp.reply_email.email.startswith(settings.smtp.reply_email.name)
            else str(settings.smtp.reply_email)
        )
    email_msg.set_content(plain_msg)
    if html_msg is not None:
        email_msg.add_alternative(html_msg, subtype="html")
    with tracer.start_as_current_span(
        "background_send_email",
        attributes={
            "subject": subject,
            "recipients": (
                [str(user.uid) for user in recipients] if isinstance(recipients, list) else str(recipients.uid)
            ),
        },
    ):
        with smtp_connection() as server:
            server.send_message(email_msg)


async def send_first_login_email(user: User) -> None:
    """
    Email a user the first time he logged in.

    Parameters
    ----------
    user : app.models.User
        The user who logged in the first time.
    """
    html, plain = EmailTemplates.FIRST_LOGIN.render(user=user)
    send_email(
        recipients=user,
        subject="S3Proxy first login",
        html_msg=html,
        plain_msg=plain,
    )
