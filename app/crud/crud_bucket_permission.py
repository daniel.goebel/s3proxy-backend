from enum import StrEnum, unique
from typing import Sequence
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import and_, delete, func, or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload
from sqlalchemy.sql import Select as SQLSelect

from app.crud.crud_bucket import CRUDBucket
from app.crud.crud_error import DuplicateError
from app.crud.crud_user import CRUDUser
from app.models import BucketPermission as BucketPermissionDB
from app.otlp import start_as_current_span_async
from app.schemas.bucket_permission import BucketPermissionIn as BucketPermissionSchema
from app.schemas.bucket_permission import BucketPermissionParameters as BucketPermissionParametersSchema

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDBucketPermission:
    @unique
    class PermissionStatus(StrEnum):
        """
        Status of a bucket permission. Can be either `ACTIVE` or `INACTIVE`. A permission can only get `INACTIVE` if the
        permission itself has a time limit and the current time is not in the timespan.
        """

        ACTIVE = "ACTIVE"
        INACTIVE = "INACTIVE"

    @staticmethod
    async def get(bucket_name: str, uid: UUID, *, db: AsyncSession) -> BucketPermissionDB | None:
        stmt = select(BucketPermissionDB).where(
            BucketPermissionDB.uid_bytes == uid.bytes, BucketPermissionDB.bucket_name == bucket_name
        )
        with tracer.start_as_current_span(
            "db_get_bucket_permission",
            attributes={"sql_query": str(stmt), "bucket_name": bucket_name, "uid": str(uid)},
        ):
            return await db.scalar(stmt)

    @staticmethod
    @start_as_current_span_async("db_list_bucket_permissions", tracer=tracer)
    async def list(
        bucket_name: str | None = None,
        uid: UUID | None = None,
        permission_types: list[BucketPermissionDB.Permission] | None = None,
        permission_status: PermissionStatus | None = None,
        *,
        db: AsyncSession,
    ) -> Sequence[BucketPermissionDB]:
        """
        Get the permissions for the given bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        bucket_name : str | None
            Name of the bucket which to query.
        uid : str | None
            UID of the user which to query.
        permission_types : list[app.models.BucketPermission.Permission] | None, default None
            Type of Bucket Permissions to fetch.
        permission_status : app.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
            Status of Bucket Permissions to fetch.

        Returns
        -------
        buckets : list[BucketPermission]
            Returns the permissions for the given bucket.
        """
        current_span = trace.get_current_span()
        stmt = select(BucketPermissionDB)

        if bucket_name is not None:
            current_span.set_attribute("bucket_name", bucket_name)
            stmt = stmt.options(joinedload(BucketPermissionDB.grantee)).where(
                BucketPermissionDB.bucket_name == bucket_name
            )
        if uid is not None:
            current_span.set_attribute("uid", str(uid))
            stmt = stmt.where(BucketPermissionDB.uid_bytes == uid.bytes)
        if permission_types is not None and len(permission_types) > 0:
            current_span.set_attribute("permission_types", [ptype.name for ptype in permission_types])
            stmt = stmt.where(or_(*[BucketPermissionDB.permissions == p_type for p_type in permission_types]))
        if permission_status is not None:
            current_span.set_attribute("permission_status", permission_status.name)
            stmt = CRUDBucketPermission._filter_permission_status(stmt, permission_status)
        current_span.set_attribute("sql_query", str(stmt))
        return (await db.scalars(stmt)).all()

    @staticmethod
    async def check_permission(bucket_name: str, uid: UUID, *, db: AsyncSession) -> bool:
        """
        Check if the provided user has any permission to the provided bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        bucket_name : str
            Name of the bucket for which to perform the check.
        uid : str
            UID of the user for which to perform the check.

        Returns
        -------
        permission_check : bool
            Return True if the user has any permission on the bucket, False otherwise.
        """
        with tracer.start_as_current_span(
            "db_check_bucket_permission",
            attributes={"uid": str(uid), "bucket_name": bucket_name},
        ):
            buckets = await CRUDBucket.get_for_user(uid, bucket_type=CRUDBucket.BucketType.ALL, db=db)
            return bucket_name in map(lambda x: x.name, buckets)

    @staticmethod
    @start_as_current_span_async("db_create_bucket_permission", tracer=tracer)
    async def create(permission: BucketPermissionSchema, *, db: AsyncSession) -> BucketPermissionDB:
        """
        Create a permission in the database and raise Exceptions if there are problems.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        permission : app.schemas.bucket_permission.BucketPermissionOut
            The permission to create.
        Returns
        -------
        permission : app.models.BucketPermission
            Newly created permission model from the db.
        """
        trace.get_current_span().set_attributes({"bucket_name": permission.bucket_name, "uid": str(permission.uid)})
        # Check if user exists
        user = await CRUDUser.get(uid=permission.uid, db=db)
        if user is None:
            raise KeyError(
                f"Unknown user with uid {str(permission.uid)}",
            )
        # Check that grantee is not the owner of the bucket
        bucket = await CRUDBucket.get(permission.bucket_name, db=db)
        if bucket is None or bucket.owner_id == user.uid:
            raise ValueError(f"User {str(permission.uid)} is the owner of the bucket {permission.bucket_name}")
        # Check if combination of user and bucket already exists
        previous_permission = await CRUDBucketPermission.get(bucket_name=permission.bucket_name, uid=user.uid, db=db)
        if previous_permission is not None:
            raise DuplicateError(
                f"bucket permission for combination {permission.bucket_name} {str(permission.uid)} already exists."
            )
        # Add permission to db
        permission_db = BucketPermissionDB(
            uid_bytes=user.uid.bytes,
            bucket_name=permission.bucket_name,
            from_=permission.from_timestamp,
            to=permission.to_timestamp,
            file_prefix=permission.file_prefix,
            permissions=permission.permission,
        )
        db.add(permission_db)
        await db.commit()
        await db.refresh(permission_db)
        return permission_db

    @staticmethod
    async def delete(bucket_name: str, uid: UUID, *, db: AsyncSession) -> None:
        """
        Delete a permission in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        bucket_name : str
            Name of the bucket.
        uid : str
            UID of the user.
        """
        stmt = delete(BucketPermissionDB).where(
            BucketPermissionDB.uid_bytes == uid.bytes, BucketPermissionDB.bucket_name == bucket_name
        )
        with tracer.start_as_current_span(
            "db_delete_bucket_permission",
            attributes={"bucket_name": bucket_name, "uid": str(uid), "sql_query": str(stmt)},
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def update_permission(
        permission: BucketPermissionDB, new_params: BucketPermissionParametersSchema, *, db: AsyncSession
    ) -> BucketPermissionDB:
        """
        Update a permission in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession
            Async database session to perform query on.
        permission : app.schemas.bucket_permission.BucketPermissionOut
            The permission to update.
        new_params : app.schemas.bucket_permission.BucketPermissionParameters
            The parameters which should be updated.

        Returns
        -------
        permission : app.models.BucketPermission
            Updated permission model from the db.
        """
        stmt = (
            update(BucketPermissionDB)
            .where(
                BucketPermissionDB.bucket_name == permission.bucket_name,
                BucketPermissionDB.uid_bytes == permission.uid.bytes,
            )
            .values(
                from_=new_params.from_timestamp,
                to=new_params.to_timestamp,
                file_prefix=new_params.file_prefix,
                permissions=new_params.permission,
            )
        )
        with tracer.start_as_current_span(
            "db_update_bucket_permission",
            attributes={"sql_query": str(stmt), "bucket_name": permission.bucket_name, "uid": str(permission.uid)},
        ):
            await db.execute(stmt)
            await db.commit()
            await db.refresh(permission)
            return permission

    @staticmethod
    def _filter_permission_status(stmt: SQLSelect, permission_status: PermissionStatus) -> SQLSelect:
        """
        Add a where clauses to the SQL Statement where the status of permission is filtered based on the current time.

        Parameters
        ----------
        stmt : sqlalchemy.sql.Select
            Declarative Select statement from SQLAlchemy
        permission_status : PermissionStatus
            Status of Bucket Permissions to filter for.

        Returns
        -------
        stmt : sqlalchemy.sql.Select
            Declarative Select statement with added where clause.
        """
        if permission_status == CRUDBucketPermission.PermissionStatus.ACTIVE:
            return stmt.where(
                or_(
                    func.UNIX_TIMESTAMP() >= BucketPermissionDB.from_,
                    BucketPermissionDB.from_ == None,  # noqa:E711
                )
            ).where(
                or_(
                    func.UNIX_TIMESTAMP() <= BucketPermissionDB.to,
                    BucketPermissionDB.to == None,  # noqa:E711
                )
            )
        else:
            return stmt.where(
                or_(
                    and_(
                        func.UNIX_TIMESTAMP() <= BucketPermissionDB.from_,
                        BucketPermissionDB.from_ != None,  # noqa:E711
                    ),
                    and_(
                        func.UNIX_TIMESTAMP() >= BucketPermissionDB.to,
                        BucketPermissionDB.to != None,  # noqa:E711
                    ),
                )
            )
