from uuid import UUID

from pydantic import BaseModel, EmailStr, Field


class UserBase(BaseModel):
    display_name: str = Field(
        ...,
        description="Full Name of the user",
        examples=["Bilbo Baggins"],
        max_length=256,
    )


class UserIn(UserBase):
    email: EmailStr = Field(..., description="Email of the user", examples=["user@example.org"])


class UserOut(UserBase):
    """
    Schema for a user.
    """

    uid: UUID = Field(
        ...,
        description="ID of the user",
        examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
    )
