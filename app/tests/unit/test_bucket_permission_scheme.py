from datetime import datetime
from uuid import uuid4

import pytest

from app.models import BucketPermission
from app.schemas.bucket_permission import BucketPermissionIn
from app.tests.utils.utils import random_lower_string


class _TestPermissionPolicy:
    @pytest.fixture(scope="function")
    def random_base_permission(self) -> BucketPermissionIn:
        """
        Generate a base READ bucket permission schema.
        """
        return BucketPermissionIn(
            uid=uuid4(),
            bucket_name=random_lower_string(),
            permission=BucketPermission.Permission.READ,
        )


class TestPermissionPolicyPermissionType(_TestPermissionPolicy):
    def test_READ_permission(self, random_base_permission: BucketPermissionIn) -> None:
        """
        Test for converting a READ Permission into a bucket policy statement.

        Parameters
        ----------
        random_base_permission : app.schemas.bucket_permission.BucketPermissionOut
            Random base bucket permission for testing.
        """
        uid = uuid4()
        stmts = random_base_permission.map_to_bucket_policy_statement(uid=uid)
        assert len(stmts) == 2

        object_stmt = stmts[0]
        assert object_stmt["Effect"] == "Allow"
        assert object_stmt["Sid"] == random_base_permission.to_hash(uid)
        assert object_stmt["Principal"]["AWS"] == f"arn:aws:iam:::user/{str(random_base_permission.uid)}"
        assert object_stmt["Resource"] == f"arn:aws:s3:::{random_base_permission.bucket_name}/*"
        with pytest.raises(KeyError):
            assert object_stmt["Condition"]
        assert len(object_stmt["Action"]) == 1
        assert object_stmt["Action"][0] == "s3:GetObject"

        bucket_stmt = stmts[1]
        assert bucket_stmt["Sid"] == random_base_permission.to_hash(uid)
        assert bucket_stmt["Effect"] == "Allow"
        assert bucket_stmt["Principal"]["AWS"] == f"arn:aws:iam:::user/{str(random_base_permission.uid)}"
        assert bucket_stmt["Resource"] == f"arn:aws:s3:::{random_base_permission.bucket_name}"
        with pytest.raises(KeyError):
            assert bucket_stmt["Condition"]
        assert len(bucket_stmt["Action"]) == 1
        assert bucket_stmt["Action"][0] == "s3:ListBucket"

    def test_WRITE_permission(self, random_base_permission: BucketPermissionIn) -> None:
        """
        Test for converting a WRITE Permission into a bucket policy statement.

        Parameters
        ----------
        random_base_permission : app.schemas.bucket_permission.BucketPermissionOut
            Random base bucket permission for testing.
        """
        random_base_permission.permission = BucketPermission.Permission.WRITE
        stmts = random_base_permission.map_to_bucket_policy_statement(uid=uuid4())
        assert len(stmts) == 2

        object_stmt = stmts[0]
        with pytest.raises(KeyError):
            assert object_stmt["Condition"]
        assert len(object_stmt["Action"]) == 2
        assert "s3:PutObject" in object_stmt["Action"]
        assert "s3:DeleteObject" in object_stmt["Action"]

        bucket_stmt = stmts[1]
        with pytest.raises(KeyError):
            assert bucket_stmt["Condition"]
        assert len(bucket_stmt["Action"]) == 2
        assert "s3:ListBucket" in bucket_stmt["Action"]
        assert "s3:DeleteObject" in bucket_stmt["Action"]

    def test_READWRITE_permission(self, random_base_permission: BucketPermissionIn) -> None:
        """
        Test for converting a READWRITE Permission into a bucket policy statement.

        Parameters
        ----------
        random_base_permission : app.schemas.bucket_permission.BucketPermissionOut
            Random base bucket permission for testing.
        """
        random_base_permission.permission = BucketPermission.Permission.READWRITE
        stmts = random_base_permission.map_to_bucket_policy_statement(uid=uuid4())
        assert len(stmts) == 2

        object_stmt = stmts[0]
        with pytest.raises(KeyError):
            assert object_stmt["Condition"]
        assert len(object_stmt["Action"]) == 3
        assert "s3:PutObject" in object_stmt["Action"]
        assert "s3:DeleteObject" in object_stmt["Action"]
        assert "s3:GetObject" in object_stmt["Action"]

        bucket_stmt = stmts[1]
        with pytest.raises(KeyError):
            assert bucket_stmt["Condition"]
        assert len(bucket_stmt["Action"]) == 2
        assert "s3:ListBucket" in bucket_stmt["Action"]
        assert "s3:DeleteObject" in bucket_stmt["Action"]


class TestPermissionPolicyCondition(_TestPermissionPolicy):
    def test_to_timestamp_condition(self, random_base_permission: BucketPermissionIn) -> None:
        """
        Test for converting a READ Permission with end time condition into a bucket policy statement.

        Parameters
        ----------
        random_base_permission : app.schemas.bucket_permission.BucketPermissionOut
            Random base bucket permission for testing.
        """
        timestamp = round(datetime.now().timestamp())
        time = datetime.fromtimestamp(timestamp)  # avoid rounding error
        random_base_permission.to_timestamp = timestamp

        stmts = random_base_permission.map_to_bucket_policy_statement(uid=uuid4())
        assert len(stmts) == 2

        object_stmt = stmts[0]
        assert object_stmt["Condition"]
        assert object_stmt["Condition"]["DateLessThan"]["aws:CurrentTime"] == time.strftime("%Y-%m-%dT%H:%M:%SZ")
        with pytest.raises(KeyError):
            assert object_stmt["Condition"]["DateGreaterThan"]

        bucket_stmt = stmts[1]
        assert bucket_stmt["Condition"]
        assert bucket_stmt["Condition"]["DateLessThan"]["aws:CurrentTime"] == time.strftime("%Y-%m-%dT%H:%M:%SZ")
        with pytest.raises(KeyError):
            assert bucket_stmt["Condition"]["DateGreaterThan"]

    def test_from_timestamp_condition(self, random_base_permission: BucketPermissionIn) -> None:
        """
        Test for converting a READ Permission with start time condition into a bucket policy statement.

        Parameters
        ----------
        random_base_permission : app.schemas.bucket_permission.BucketPermissionOut
            Random base bucket permission for testing.
        """
        time = datetime.now()
        timestamp = round(datetime.now().timestamp())
        time = datetime.fromtimestamp(timestamp)  # avoid rounding error
        random_base_permission.from_timestamp = timestamp

        stmts = random_base_permission.map_to_bucket_policy_statement(uid=uuid4())
        assert len(stmts) == 2

        object_stmt = stmts[0]
        assert object_stmt["Condition"]
        assert object_stmt["Condition"]["DateGreaterThan"]["aws:CurrentTime"] == time.strftime("%Y-%m-%dT%H:%M:%SZ")
        with pytest.raises(KeyError):
            assert object_stmt["Condition"]["DateLessThan"]

        bucket_stmt = stmts[1]
        assert bucket_stmt["Condition"]
        assert bucket_stmt["Condition"]["DateGreaterThan"]["aws:CurrentTime"] == time.strftime("%Y-%m-%dT%H:%M:%SZ")
        with pytest.raises(KeyError):
            assert bucket_stmt["Condition"]["DateLessThan"]

    def test_file_prefix_condition(self, random_base_permission: BucketPermissionIn) -> None:
        """
        Test for converting a READ Permission with file prefix condition into a bucket policy statement.

        Parameters
        ----------
        random_base_permission : app.schemas.bucket_permission.BucketPermissionOut
            Random base bucket permission for testing.
        """
        random_base_permission.file_prefix = random_lower_string(length=8) + "/" + random_lower_string(length=8) + "/"

        stmts = random_base_permission.map_to_bucket_policy_statement(uid=uuid4())
        assert len(stmts) == 2

        object_stmt = stmts[0]
        assert (
            object_stmt["Resource"]
            == f"arn:aws:s3:::{random_base_permission.bucket_name}/{random_base_permission.file_prefix}*"
        )
        with pytest.raises(KeyError):
            assert object_stmt["Condition"]

        bucket_stmt = stmts[1]
        assert bucket_stmt["Condition"]
        assert bucket_stmt["Condition"]["StringLike"]["s3:prefix"] == random_base_permission.file_prefix + "*"
