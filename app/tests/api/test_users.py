import random
import uuid

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter

from app.schemas.user import UserOut
from app.tests.utils.user import UserWithAuthHeader


class _TestUserRoutes:
    base_path = "/users"


UserList = TypeAdapter(list[UserOut])


class TestUserRoutesGet(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_get_user_me(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting the currently logged-in user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : appdb.models.User
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/me", headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_200_OK
        current_user = UserOut.model_validate_json(response.content)
        assert current_user.uid == random_user.user.uid
        assert current_user.display_name == random_user.user.display_name

    @pytest.mark.asyncio
    async def test_get_unknown_user(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting an unknown user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(f"{self.base_path}/{uuid.uuid4()}", headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_user_by_uid(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting a known user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : appdb.models.User
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/{str(random_user.user.uid)}", headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_200_OK
        current_user = UserOut.model_validate_json(response.content)
        assert current_user.uid == random_user.user.uid
        assert current_user.display_name == random_user.user.display_name

    @pytest.mark.asyncio
    async def test_search_user_by_name_substring(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test for searching a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : appdb.models.User
            Random user for testing.
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """
        substring_indices = [0, 0]
        while substring_indices[1] - substring_indices[0] < 3:
            substring_indices = sorted(random.choices(range(len(random_user.user.display_name)), k=2))

        random_substring = random_user.user.display_name[substring_indices[0] : substring_indices[1]]

        response = await client.get(
            f"{self.base_path}", params={"name_like": random_substring}, headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_200_OK
        users = UserList.validate_json(response.content)

        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.user.uid) == 1

    @pytest.mark.asyncio
    async def test_list_all_user(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test for searching a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : appdb.models.User
            Random user for testing..
        user_token_headers : dict[str,str]
            HTTP Headers to authorize the request.
        """

        response = await client.get(f"{self.base_path}", headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_200_OK

        users = UserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.user.uid) == 1
