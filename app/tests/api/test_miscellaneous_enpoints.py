import pytest
from fastapi import status
from httpx import AsyncClient


class TestHealthRoute:
    @pytest.mark.asyncio
    async def test_health_route(self, client: AsyncClient) -> None:
        """
        Test service health route

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get("/health")
        assert response.status_code == status.HTTP_200_OK
        body = response.json()
        assert body["status"] == "OK"
