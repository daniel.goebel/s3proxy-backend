from datetime import datetime, timedelta

import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDBucket, DuplicateError
from app.models import Bucket, BucketPermission
from app.schemas.bucket import BucketIn
from app.tests.utils.bucket import add_permission_for_bucket, delete_bucket
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_lower_string


class TestBucketCRUDGet:
    @pytest.mark.asyncio
    async def test_get_all_bucket(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for getting all buckets from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        """
        buckets = await CRUDBucket.get_all(db=db)
        assert len(buckets) == 1
        bucket = buckets[0]
        assert bucket.name == random_bucket.name
        assert bucket.public == random_bucket.public
        assert bucket.description == random_bucket.description

    @pytest.mark.asyncio
    async def test_get_bucket_by_name(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for getting a bucket by name from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        """
        bucket = await CRUDBucket.get(random_bucket.name, db=db)
        assert bucket
        assert bucket.name == random_bucket.name
        assert bucket.public == random_bucket.public
        assert bucket.description == random_bucket.description

    @pytest.mark.asyncio
    async def test_get_unknown_bucket(self, db: AsyncSession) -> None:
        """
        Test for getting a not existing bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        bucket = await CRUDBucket.get("unknown Bucket", db=db)
        assert bucket is None

    @pytest.mark.asyncio
    async def test_get_only_own_buckets(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for getting only the buckets where a user is the owner from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        """
        assert random_bucket.owner_id is not None
        buckets = await CRUDBucket.get_for_user(random_bucket.owner_id, CRUDBucket.BucketType.OWN, db=db)

        assert len(buckets) == 1
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_only_foreign_bucket(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for getting only foreign buckets with permissions for a user from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket = Bucket(
            name=random_lower_string(),
            description=random_lower_string(127),
            owner_id_bytes=random_second_user.user.uid_bytes,
        )
        db.add(bucket)
        await db.commit()
        cleanup.add_task(
            delete_bucket,
            db=db,
            bucket_name=bucket.name,
        )
        assert random_bucket.owner_id is not None
        await add_permission_for_bucket(
            db, bucket.name, random_bucket.owner_id, permission=BucketPermission.Permission.READ
        )

        assert random_bucket.owner_id is not None
        buckets = await CRUDBucket.get_for_user(random_bucket.owner_id, CRUDBucket.BucketType.PERMISSION, db=db)
        assert len(buckets) == 1
        assert buckets[0] != random_bucket
        assert buckets[0].name == bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_read_permission_and_own(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthHeader,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for getting the users own bucket and a foreign bucket with READ permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket = Bucket(
            name=random_lower_string(),
            description=random_lower_string(127),
            owner_id_bytes=random_second_user.user.uid_bytes,
        )
        db.add(bucket)
        await db.commit()
        cleanup.add_task(
            delete_bucket,
            db=db,
            bucket_name=bucket.name,
        )
        assert random_bucket.owner_id is not None
        await add_permission_for_bucket(
            db, bucket.name, random_bucket.owner_id, permission=BucketPermission.Permission.READ
        )

        buckets = await CRUDBucket.get_for_user(random_bucket.owner_id, db=db)

        assert len(buckets) == 2
        assert buckets[0].name == random_bucket.name or buckets[1].name == random_bucket.name
        assert buckets[0].name == bucket.name or buckets[1].name == bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_read_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a foreign bucket with READ permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, permission=BucketPermission.Permission.READ
        )

        buckets = await CRUDBucket.get_for_user(random_second_user.user.uid, db=db)

        assert len(buckets) > 0
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_readwrite_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a foreign bucket with READWRITE permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, permission=BucketPermission.Permission.READWRITE
        )

        buckets = await CRUDBucket.get_for_user(random_second_user.user.uid, db=db)

        assert len(buckets) > 0
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_write_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a foreign bucket with WRITE permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, permission=BucketPermission.Permission.WRITE
        )

        buckets = await CRUDBucket.get_for_user(random_second_user.user.uid, db=db)

        assert len(buckets) == 1
        assert buckets[0] == random_bucket

    @pytest.mark.asyncio
    async def test_get_bucket_with_valid_time_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a foreign bucket with valid time permission from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_second_user.user.uid,
            from_=datetime.now() - timedelta(days=10),
            to=datetime.now() + timedelta(days=10),
        )

        buckets = await CRUDBucket.get_for_user(random_second_user.user.uid, db=db)

        assert len(buckets) > 0
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_invalid_from_time_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a foreign bucket with invalid 'from' time permission from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, from_=datetime.now() + timedelta(days=10)
        )

        buckets = await CRUDBucket.get_for_user(random_second_user.user.uid, db=db)

        assert len(buckets) == 0

    @pytest.mark.asyncio
    async def test_get_bucket_with_invalid_to_time_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthHeader
    ) -> None:
        """
        Test for getting a foreign bucket with invalid 'to' time permission from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.user.uid, to=datetime.now() - timedelta(days=10)
        )

        buckets = await CRUDBucket.get_for_user(random_second_user.user.uid, db=db)

        assert len(buckets) == 0


class TestBucketCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_bucket(
        self,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for creating a bucket with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket_info = BucketIn(name=random_lower_string(), description=random_lower_string(127))
        bucket = await CRUDBucket.create(bucket_info, random_user.user.uid, db=db)
        cleanup.add_task(
            delete_bucket,
            db=db,
            bucket_name=bucket.name,
        )
        assert bucket.name == bucket_info.name
        assert bucket.owner_id == random_user.user.uid
        assert bucket.description == bucket_info.description

        stmt = select(Bucket).where(Bucket.name == bucket_info.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db
        assert bucket_db.name == bucket_info.name
        assert bucket_db.owner_id == random_user.user.uid
        assert bucket_db.description == bucket_info.description

    @pytest.mark.asyncio
    async def test_create_duplicated_bucket(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for creating a duplicated bucket with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        """
        bucket_info = BucketIn(name=random_bucket.name, description=random_lower_string(127))
        assert random_bucket.owner_id is not None
        with pytest.raises(DuplicateError):
            await CRUDBucket.create(bucket_info, random_bucket.owner_id, db=db)


class TestBucketCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_public_state(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for updating the bucket public state with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        """
        old_public_state = random_bucket.public
        await CRUDBucket.update_public_state(db=db, bucket_name=random_bucket.name, public=not old_public_state)

        stmt = select(Bucket).where(Bucket.name == random_bucket.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db is not None
        assert bucket_db == random_bucket
        assert old_public_state != bucket_db.public

    @pytest.mark.asyncio
    async def test_update_bucket_limits(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for updating the bucket limits with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        """
        await CRUDBucket.update_bucket_limits(db=db, bucket_name=random_bucket.name, size_limit=100, object_limit=120)

        stmt = select(Bucket).where(Bucket.name == random_bucket.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db is not None
        assert bucket_db == random_bucket
        assert bucket_db.size_limit is not None
        assert bucket_db.size_limit == 100

        assert bucket_db.object_limit is not None
        assert bucket_db.object_limit == 120


class TestBucketCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_bucket(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for deleting a bucket with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : app.models.Bucket
            Random bucket for testing.
        """
        await CRUDBucket.delete(random_bucket.name, db=db)

        stmt = select(Bucket).where(Bucket.name == random_bucket.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db is None
