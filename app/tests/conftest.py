import asyncio
import json
from typing import Any, AsyncIterator, Generator

import httpx
import pytest
import pytest_asyncio
from fastapi import status
from fastapi.responses import RedirectResponse
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

from app.api import dependencies
from app.api.dependencies import LoginException, get_db, get_rgw_admin, get_s3_resource, verify_and_fetch_userinfo
from app.core import security
from app.core.config import settings
from app.main import app
from app.models import Bucket
from app.models import BucketPermission as BucketPermissionDB
from app.schemas.bucket_permission import BucketPermissionOut as BucketPermissionSchema
from app.schemas.security import UserInfo
from app.tests.mocks import MockRGWAdmin, MockS3ServiceResource
from app.tests.utils.bucket import create_random_bucket
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader, create_random_user, get_authorization_headers


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    """
    Creates an instance of the default event loop for the test session.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def mock_rgw_admin() -> MockRGWAdmin:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockRGWAdmin()


@pytest.fixture(scope="session")
def mock_s3_service() -> MockS3ServiceResource:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockS3ServiceResource()


@pytest.fixture(autouse=True)
def monkeypatch_background_dependencies(
    monkeypatch: pytest.MonkeyPatch, mock_s3_service: MockS3ServiceResource, mock_rgw_admin: MockRGWAdmin
) -> None:
    """
    Fixture to eliminate the dependency of a mock OIDC service
    """

    class MockProvider:
        async def authorize_redirect(self, *args: Any, **kwargs: Any) -> RedirectResponse:
            return RedirectResponse(
                url=str(settings.lifescience_oidc.base_uri),
                status_code=status.HTTP_302_FOUND,
            )

    monkeypatch.setattr(security, "get_provider", lambda x: MockProvider())
    monkeypatch.setattr(dependencies, "get_rgw_admin", lambda: mock_rgw_admin)
    monkeypatch.setattr(dependencies, "get_s3_resource", lambda: mock_s3_service)


@pytest_asyncio.fixture(scope="module")
async def client(
    mock_s3_service: MockS3ServiceResource,
    db: AsyncSession,
    mock_rgw_admin: MockRGWAdmin,
) -> AsyncIterator[httpx.AsyncClient]:
    """
    Fixture for creating a TestClient and perform HTTP Request on it.
    Overrides several dependencies.
    """

    async def get_mock_userinfo(sub: str, name: str, email: str = "", error: bool = False) -> UserInfo:
        if error:
            raise LoginException(error_source="mock_error")
        return UserInfo(sub=sub + "@lifescience-ri.eu", name=name, email=email)

    app.dependency_overrides[get_rgw_admin] = lambda: mock_rgw_admin
    app.dependency_overrides[get_s3_resource] = lambda: mock_s3_service
    app.dependency_overrides[verify_and_fetch_userinfo] = get_mock_userinfo
    app.dependency_overrides[get_db] = lambda: db
    async with httpx.AsyncClient(transport=httpx.ASGITransport(app=app), base_url="http://localhost") as ac:  # type: ignore[arg-type]
        yield ac
    app.dependency_overrides = {}


@pytest_asyncio.fixture(scope="module")
async def db() -> AsyncIterator[AsyncSession]:
    """
    Fixture for creating a database session to connect to.
    """
    engine_async = create_async_engine(str(settings.db.dsn_async), echo=settings.db.verbose, pool_recycle=3600)
    async with async_sessionmaker(engine_async, expire_on_commit=False)() as session:
        yield session
    await engine_async.dispose()


@pytest_asyncio.fixture(scope="function")
async def random_user(db: AsyncSession, mock_rgw_admin: MockRGWAdmin) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random user and deletes him afterwards.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid))
    mock_rgw_admin.delete_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_second_user(db: AsyncSession, mock_rgw_admin: MockRGWAdmin) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random second user and deletes him afterwards.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid))
    mock_rgw_admin.delete_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_third_user(db: AsyncSession, mock_rgw_admin: MockRGWAdmin) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random third user and deletes him afterwards.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid))
    mock_rgw_admin.delete_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket(
    db: AsyncSession, random_user: UserWithAuthHeader, mock_s3_service: MockS3ServiceResource
) -> AsyncIterator[Bucket]:
    """
    Create a random bucket and deletes it afterwards.
    """
    bucket = await create_random_bucket(db, random_user.user)
    mock_s3_service.Bucket(name=bucket.name).create()
    mock_s3_service.BucketPolicy(bucket.name).put(
        json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "PseudoOwnerPerm",
                        "Effect": "Allow",
                        "Principal": {"AWS": [f"arn:aws:iam:::user/{str(random_user.user.uid)}"]},
                        "Action": ["s3:GetObject", "s3:DeleteObject", "s3:PutObject", "s3:ListBucket"],
                        "Resource": [f"arn:aws:s3:::{bucket.name}/*", f"arn:aws:s3:::{bucket.name}"],
                    }
                ],
            }
        )
    )
    yield bucket
    mock_s3_service.delete_bucket(name=bucket.name, force_delete=True)
    await db.delete(bucket)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket_permission(
    db: AsyncSession,
    random_second_user: UserWithAuthHeader,
    random_bucket: Bucket,
    mock_s3_service: MockS3ServiceResource,
) -> BucketPermissionDB:
    """
    Create a bucket READ permission for the second user on a bucket.
    """
    permission_db = BucketPermissionDB(uid_bytes=random_second_user.user.uid.bytes, bucket_name=random_bucket.name)
    db.add(permission_db)
    await db.commit()
    await db.refresh(permission_db)
    mock_s3_service.Bucket(random_bucket.name).Policy().put(
        json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": BucketPermissionSchema.from_db_model(permission_db).map_to_bucket_policy_statement(
                    random_second_user.user.uid
                ),
            }
        )
    )
    return permission_db


@pytest_asyncio.fixture(scope="function")
async def random_bucket_permission_schema(
    random_bucket_permission: BucketPermissionDB, random_second_user: UserWithAuthHeader
) -> BucketPermissionSchema:
    """
    Create a bucket READ permission for the second user on a bucket.
    """

    return BucketPermissionSchema.from_db_model(random_bucket_permission)


@pytest_asyncio.fixture(scope="function")
async def cleanup(db: AsyncSession) -> AsyncIterator[CleanupList]:
    """
    Yields a Cleanup object where (async) functions can be registered which get executed after a (failed) test
    """
    cleanup_list = CleanupList()
    yield cleanup_list
    await cleanup_list.empty_queue()
