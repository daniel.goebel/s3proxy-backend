import json
from datetime import datetime
from uuid import UUID

from sqlalchemy import delete, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.endpoints.buckets import get_anonymously_bucket_policy
from app.models import Bucket, BucketPermission, User
from app.tests.mocks.mock_s3_resource import MockS3ServiceResource

from .utils import random_lower_string


async def delete_bucket(db: AsyncSession, bucket_name: str) -> None:
    await db.execute(
        delete(Bucket).where(
            Bucket.name == bucket_name,
        )
    )
    await db.commit()


async def create_random_bucket(db: AsyncSession, user: User) -> Bucket:
    """
    Creates a random bucket in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    user : app.models.User
        Owner of the bucket.

    Returns
    -------
    bucket : app.models.Bucket
        Newly created bucket.
    """
    bucket = Bucket(
        name=random_lower_string(),
        description=random_lower_string(length=127),
        owner_id_bytes=user.uid_bytes,
    )
    db.add(bucket)
    await db.commit()
    return bucket


async def add_permission_for_bucket(
    db: AsyncSession,
    bucket_name: str,
    uid: UUID,
    from_: datetime | None = None,
    to: datetime | None = None,
    permission: BucketPermission.Permission = BucketPermission.Permission.READ,
) -> None:
    """
    Creates Permission to a bucket for a user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    bucket_name : str
        name of the bucket.
    uid : str
        UID of the user who we grant the permission.
    from_ : datetime.datetime | None, default None
        Time when from when the permission should be active.
    to : datetime.datetime | None, default None
        Time till when the permissions should be active.
    permission : app.models.BucketPermission.Permission, default BucketPermission.Permission.READ  # noqa:E501
        The permission the user is granted.
    """
    perm = BucketPermission(
        uid_bytes=uid.bytes,
        bucket_name=bucket_name,
        from_=round(from_.timestamp()) if from_ is not None else None,
        to=round(to.timestamp()) if to is not None else None,
        permissions=permission.name,
    )
    db.add(perm)
    await db.commit()


async def make_bucket_public(db: AsyncSession, bucket_name: str, s3: MockS3ServiceResource) -> None:
    """
    Creates Permission to a bucket for a user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    bucket_name : str
        Name of the bucket.
    s3 : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
        Mock S3 Service to manipulate objects.
    """
    await db.execute(update(Bucket).where(Bucket.name == bucket_name).values(public=True))
    await db.commit()
    policy = json.loads(s3.Bucket(bucket_name).Policy().policy)
    policy["Statement"] += get_anonymously_bucket_policy(bucket_name)
    s3.Bucket(bucket_name).Policy().put(Policy=json.dumps(policy))
