from dataclasses import dataclass
from uuid import UUID

import pytest
from authlib.jose import JsonWebToken
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.security import create_access_token
from app.models import User

from .utils import random_lower_string

_jwt = JsonWebToken(["HS256"])


@dataclass
class UserWithAuthHeader:
    auth_headers: dict[str, str]
    user: User


def get_authorization_headers(uid: UUID) -> dict[str, str]:
    """
    Create a valid JWT and return the correct headers for subsequent requests.

    Parameters
    ----------
    uid : str
        UID of the user who should be logged in.

    Returns
    -------
    headers : dict[str,str]
        HTTP Headers to authorize each request.
    """
    return {"Authorization": f"Bearer {create_access_token(str(uid))}"}


@pytest.mark.asyncio
async def create_random_user(db: AsyncSession) -> User:
    """
    Creates a random user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.

    Returns
    -------
    user : app.models.User
        Newly created user.
    """
    user = User(
        lifescience_id=random_lower_string(),
        display_name=random_lower_string(),
    )
    db.add(user)
    await db.commit()
    return user
