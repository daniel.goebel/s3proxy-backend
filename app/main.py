from brotli_asgi import BrotliMiddleware
from fastapi import FastAPI, Request, Response, status
from fastapi.exception_handlers import http_exception_handler, request_validation_exception_handler
from fastapi.exceptions import RequestValidationError, StarletteHTTPException
from fastapi.responses import JSONResponse, RedirectResponse
from fastapi.routing import APIRoute
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace import Status, StatusCode
from pydantic import AnyHttpUrl
from starlette.middleware.sessions import SessionMiddleware

from app.api import api_router
from app.api.dependencies import LoginException
from app.core.config import settings

description = """
This is the s3 proxy service.
"""


def custom_generate_unique_id(route: APIRoute) -> str:
    return f"{route.tags[-1]}-{route.name}"


app = FastAPI(
    title="S3-Proxy Service",
    version="1.0.0",
    description=description,
    contact={
        "name": "Daniel Goebel",
        "url": "https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp?personId=223066601",
        "email": "dgoebel@techfak.uni-bielefeld.de",
    },
    generate_unique_id_function=custom_generate_unique_id,
    license_info={"name": "Apache 2.0", "url": "https://www.apache.org/licenses/LICENSE-2.0"},
    root_path=settings.api_prefix,
)
if settings.api_prefix:  # pragma: no cover
    app.servers.insert(0, {"url": settings.api_prefix})

if settings.otlp.grpc_endpoint is not None and len(settings.otlp.grpc_endpoint) > 0:  # pragma: no cover
    resource = Resource(attributes={SERVICE_NAME: "s3proxy-service"})
    provider = TracerProvider(resource=resource)
    provider.add_span_processor(
        BatchSpanProcessor(OTLPSpanExporter(endpoint=settings.otlp.grpc_endpoint, insecure=not settings.otlp.secure))
    )
    trace.set_tracer_provider(provider)

    @app.exception_handler(StarletteHTTPException)
    async def trace_http_exception_handler(request: Request, exc: StarletteHTTPException) -> Response:
        current_span = trace.get_current_span()
        current_span.set_status(Status(StatusCode.ERROR))
        current_span.record_exception(exc)
        return await http_exception_handler(request, exc)

    @app.exception_handler(RequestValidationError)
    async def trace_validation_exception_handler(request: Request, exc: RequestValidationError) -> JSONResponse:
        current_span = trace.get_current_span()
        current_span.set_status(Status(StatusCode.ERROR))
        current_span.record_exception(exc)
        return await request_validation_exception_handler(request, exc)


FastAPIInstrumentor.instrument_app(
    app, excluded_urls="health,docs,openapi.json", tracer_provider=trace.get_tracer_provider()
)

# Enable br compression for large responses, fallback gzip
app.add_middleware(BrotliMiddleware)
app.add_middleware(SessionMiddleware, secret_key=settings.secret_key, https_only=settings.ui_uri.scheme == "https")

# Include all routes
app.include_router(api_router)


@app.exception_handler(LoginException)
def login_exception_handler(request: Request, exc: LoginException) -> Response:
    """
    Exception handler for all kinds of login errors.

    Parameters
    ----------
    request : fastapi.Request
        Original request where the exception occurred.
    exc : LoginException
        The exception that was raised.

    Returns
    -------
    redirect : fastapi.Response
        Redirect to base URL with error as query parameter
    """
    return RedirectResponse(
        str(
            AnyHttpUrl.build(
                scheme=settings.ui_uri.scheme,
                host=settings.ui_uri.host,  # type: ignore[arg-type]
                port=settings.ui_uri.port,
                path=None if settings.ui_uri.path is None else settings.ui_uri.path.strip("/"),
                query=f"login_error={exc.error_source}",
            )
        ),
        status_code=status.HTTP_302_FOUND,
    )
