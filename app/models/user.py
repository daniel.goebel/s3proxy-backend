from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import BINARY, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base_class import Base, uuid7

if TYPE_CHECKING:
    from .bucket import Bucket
    from .bucket_permission import BucketPermission


class User(Base):
    """
    Database model for a user.
    """

    __tablename__: str = "user"
    uid_bytes: Mapped[bytes] = mapped_column(
        BINARY(16),
        name="uid",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7().bytes,
    )
    lifescience_id: Mapped[str | None] = mapped_column(String(64), index=True, unique=True, nullable=True)
    display_name: Mapped[str] = mapped_column(String(256), nullable=False)
    email: Mapped[str | None] = mapped_column(String(256), nullable=True)
    buckets: Mapped[list["Bucket"]] = relationship("Bucket", back_populates="owner")
    permissions: Mapped[list["BucketPermission"]] = relationship(
        "BucketPermission",
        back_populates="grantee",
        cascade="all, delete",
        passive_deletes=True,
    )

    @property
    def uid(self) -> UUID:
        return UUID(bytes=self.uid_bytes)

    def __eq__(self, other: Any) -> bool:
        return self.uid == other.uid if isinstance(other, User) else False

    def __repr__(self) -> str:
        return f"'User(uid={self.uid}', display_name='{self.display_name}')"
