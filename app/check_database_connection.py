import logging

from alembic import command, config
from pymysql.err import ProgrammingError
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from app.core.config import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 30  # 3*30 seconds
wait_seconds = 3


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    engine = create_engine(str(settings.db.dsn_sync), pool_pre_ping=True, pool_recycle=3600, echo=False)
    try:
        with sessionmaker(autocommit=False, autoflush=False, bind=engine)() as db:
            # Try to create session to check if DB is awake
            try:
                db.execute(text("SELECT version_num FROM alembic_version LIMIT 1")).scalar_one_or_none()
            except ProgrammingError as e:
                logger.error(e)
            finally:
                alembic_config = config.Config("./alembic.ini")
                command.upgrade(alembic_config, "head")
    except Exception as e:
        logger.error(e)
        raise e
    finally:
        engine.dispose()


def main() -> None:
    logger.info("Check DB connection")
    init()
    logger.info("DB connected and up to date")


if __name__ == "__main__":
    main()
