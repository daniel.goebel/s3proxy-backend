from uuid import UUID

from opentelemetry import trace
from pydantic import ByteSize
from rgwadmin import RGWAdmin

from app.core.config import settings
from app.models import Bucket
from app.schemas.s3key import S3Key

tracer = trace.get_tracer_provider().get_tracer(__name__)

__all__ = ["rgw", "get_s3_keys", "update_bucket_limits"]

rgw = RGWAdmin(
    access_key=settings.s3.access_key,
    secret_key=settings.s3.secret_key.get_secret_value(),
    secure=settings.s3.uri.scheme == "https",
    server=str(settings.s3.uri).split("://")[-1][:-1],
)


def get_s3_keys(rgw: RGWAdmin, uid: UUID) -> list[S3Key]:
    with tracer.start_as_current_span("s3_get_user_keys", attributes={"uid": str(uid)}):
        return [S3Key(uid=uid, **key) for key in rgw.get_user(uid=str(uid), stats=False)["keys"]]


def update_bucket_limits(rgw: RGWAdmin, bucket: Bucket) -> None:
    with tracer.start_as_current_span(
        "rgw_set_bucket_limits",
        attributes={
            "bucket_name": bucket.name,
            "enabled": bucket.object_limit is not None or bucket.size_limit is not None,
        },
    ) as span:
        if bucket.size_limit is not None:  # pragma: no cover
            span.set_attribute("size_limit", ByteSize(bucket.size_limit * 1024).human_readable())
        if bucket.object_limit is not None:  # pragma: no cover
            span.set_attribute("object_limit", bucket.object_limit)
        rgw.set_bucket_quota(
            uid=settings.s3.username,
            bucket=bucket.name,
            max_size_kb=-1 if bucket.size_limit is None else bucket.size_limit,
            max_objects=-1 if bucket.object_limit is None else bucket.object_limit,
            enabled=bucket.object_limit is not None or bucket.size_limit is not None,
        )
