# S3Proxy Backend

## Dev Setup

### Python Environment

Python `>=3.11` is required.

```shell
pip install virtualenv
virtualenv venv
source venv/bin/activate
python -m pip install --upgrade -r requirements.txt -r requirements-dev.txt
pre-commit install
```

### Dev Database

```shell
docker volume create s3proxy-db
docker run --name s3proxy-dev-db -d \
-p 127.0.0.1:3306:3306 \
-e MYSQL_RANDOM_ROOT_PASSWORD=yes \
-e MYSQL_DATABASE=s3proxydb \
-e MYSQL_USER=s3proxy \
-e MYSQL_PASSWORD=password \
-v s3proxy-db:/var/lib/mysql \
mysql:8
```

### Test Database

```shell
docker run --name s3proxy-test-db -d \
-p 127.0.0.1:8306:3306 \
-e MYSQL_RANDOM_ROOT_PASSWORD=yes \
-e MYSQL_DATABASE=s3proxydb \
-e MYSQL_USER=s3proxy \
-e MYSQL_PASSWORD=password \
mysql:8
```

### Ceph setup

Create a ceph cluster described in [ceph/README.md](ceph/README.md).
Open a cephadm shell `sudo cephadm shell --`

```shell
radosgw-admin user create --uid=bucket-manager --display-name="S3Proxy Bucket Manager"
radosgw-admin caps add --uid=bucket-manager --caps="users=*;buckets=*"
```

Save the generated S3 keys

### Dev OIDC

Look at the [README.md](dev-oidc/README.md) in the folder `dev-oidc` and add yourself as user to the OIDC Provider. Then
start the container

```shell
cd dev-oidc
docker run --name s3proxy-dev-oidc -d \
  -p 127.0.0.1:8001:80 \
  -v $(pwd):/tmp/config:ro \
  -e ASPNETCORE_ENVIRONMENT=Development \
  -e ASPNET_SERVICES_OPTIONS_PATH=/tmp/config/service_options.json \
  -e CLIENTS_CONFIGURATION_PATH=/tmp/config/clients_config.json \
  -e IDENTITY_RESOURCES_PATH=/tmp/config/identity_resources.json \
  -e SERVER_OPTIONS_PATH=/tmp/config/server_options.json \
  -e USERS_CONFIGURATION_PATH=/tmp/config/users_config.json \
  ghcr.io/soluto/oidc-server-mock:0.8.6
```

### Jaeger (optional)

The service can send trace via gRPC to a OpenTelemetry compatible endpoint. [Jaeger](https://www.jaegertracing.io) is
one example of such a service.

```shell
docker run --name s3proxy-dev-jaeger -d \
-p 127.0.0.1:4317:4317 \ # gRPC endpoint
-p 127.0.0.1:16686:16686 \ # UI endpoint
jaegertracing/all-in-one:latest
```

Visit http://localhost:16686 so see the traces.

### Reverse Proxy

The backend and frontend must be service under the same host. In `dev-traefik` is the config file for the
reverse-proxy [Traefik](https://doc.traefik.io/traefik).

```shell
cd dev-traefik
curl -L -o traefik.tar.gz https://github.com/traefik/traefik/releases/download/v3.0.0/traefik_v3.0.0_linux_amd64.tar.gz
tar -xf traefik.tar.gz && rm traefik.tar.gz
```

### Create Dev config

Create a file `config.yaml` with the following content and fill in the saved S3 keys from the ceph setup and the
endpoint for the RGW.

```yaml
db:
  port: 3306
  host: "localhost"
  user: "s3proxy"
  name: "s3proxydb"
  password: "password"
  verbose: false
s3:
  uri: "<rgw url>"
  access_key: "<access-key>"
  secret_key: "<secret-key>"
  username: "bucket-manager"
smtp:
  server: "console"
secret_key: "some-super-secret-key"
ui_uri: "http://localhost:9999"
block_foreign_users: false
lifescience_oidc:
  client_secret: "e6430d37978a295fd8dd6ce19865b24a76acc797"
  client_id: "066ba98b893f8e96f20d"
  base_uri: "http://localhost:8001"
otlp: # only add this block, if you are using jaeger
  grpc_endpoint: "localhost:4317"
```

## Running the dev system

### Start reverse proxy

```shell
cd dev-traefik
./traefik --provider.file.filename=traefik.toml
```

### Start Backend

```shell
export PYTHONPATH=$PWD
./scripts/prestart.sh
uvicorn app.main:app
```

### Start Frontend

Node 20 is required.

```shell
cd ../s3proxy-ui
npm install
npm run dev
```

Visit http://localhsot:9999.

## Run tests

Create a file `test-config.yaml` with the following content.

```yaml
db:
  port: 8306
  host: "localhost"
  user: "s3proxy"
  name: "s3proxydb"
  password: "password"
  verbose: false
s3:
  uri: "http://localhost"
  access_key: "nonempty"
  secret_key: "nonempty"
  username: "nonempty"
ui_uri: "http://localhost"
block_foreign_users: false
lifescience_oidc:
  client_secret: "nonempty"
  client_id: "nonempty"
  base_uri: "http://localhost"
```

```shell
export PYTHONPATH=$PWD
export S3PROXY_CONFIG_FILE_YAML=test-config.yaml
python app/check_database_connection.py
python -m pytest --cov=app --cov-report=html app/tests
```

## CI

This repository has a GitLab pipeline configured that automatically run the linters and tests on each git commit.
To skip the CI pipeline on a commit, see
the [GitLab documentation](https://docs.gitlab.com/ee/ci/pipelines/#skip-a-pipeline).

If there is a new commit on the main branch it will trigger a job that builds and publish the docker containers on the
repository container registry.
