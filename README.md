# S3Proxy Service

## Description

Openstack is shipping with an integrated UI to access the Object Store provided by Ceph. Unfortunately, this UI does not
allow fine-grained control who can access a bucket or object. You can either make it accessible for everyone or nobody,
but Ceph can do this and much more. 👎
This is the backend for a new UI which can leverage the additional powerful functionality provided by Ceph in a
user-friendly manner. 👍

| Feature                     | Openstack Integration | New UI |
|-----------------------------|:---------------------:|:------:|
| Create / Delete Buckets UI  |           ✅           |   ✅    |
| Create / Delete Buckets CLI |           ✅           |   ❌    |
| Upload / Download Objects   |           ✅           |   ✅    |
| Fine-grained Access Control |           ❌           |   ✅    |

### Concept

![Visualization of Concept](figures/cloud_object_storage.svg)

### Developing

See [DEVELOPING.md](DEVELOPING.md) for a guide how to set up a developing instance.

## Configuration

### General

| Env variable               | Config file key | Default       | Value    | Example                | Description                                                                                                           |
|----------------------------|-----------------|---------------|----------|------------------------|-----------------------------------------------------------------------------------------------------------------------|
| `S3PROXY_CONFIG_FILE_YAML` | -               | `config.yaml` | Filepath | `/path/to/config.yaml` | Path to a YAML file to read the config. See [example-config/example-config.yaml](example-config/example-config.yaml). |
| `S3PROXY_CONFIG_FILE_TOML` | -               | `config.toml` | Filepath | `/path/to/config.toml` | Path to a TOML file to read the config. See [example-config/example-config.toml](example-config/example-config.toml). |
| `S3PROXY_CONFIG_FILE_JSON` | -               | `config.json` | Filepath | `/path/to/config.json` | Path to a JSON file to read the config. See [example-config/example-config.json](example-config/example-config.json). |
| `S3PROXY_API_PREFIX`       | `api_prefix`    | unset         | URI path | `/api`                 | Prefix before every URL path                                                                                          |
| * `S3PROXY_UI_URI`         | `ui_uri`        | unset         | HTTP URL | `https://localhost`    | HTTP URL of the S3Proxy website                                                                                       |

### Database

| Env variable             | Config file key | Default     | Value              | Example       | Description                                                    |
|--------------------------|-----------------|-------------|--------------------|---------------|----------------------------------------------------------------|
| `S3PROXY_DB__HOST`       | `db.host`       | `localhost` | <db hostname / IP> | `localhost`   | IP or Hostname Address of DB                                   |
| `S3PROXY_DB__PORT`       | `db.port`       | 3306        | Integer            | 3306          | Port of the database                                           |
| * `S3PROXY_DB__USER`     | `db.user`       | unset       | String             | `db-user`     | Username of the database user                                  |
| * `S3PROXY_DB__PASSWORD` | `db.password`   | unset       | String             | `db-password` | Password of the database user                                  |
| * `S3PROXY_DB__NAME`     | `db.name`       | unset       | String             | `db-name`     | Name of the database                                           |
| `S3PROXY_DB__VERBOSE`    | `db.verbose`    | `false`     | Boolean            | `false`       | Enables verbose SQL output.<br>Should be `false` in production |

### S3

| Env variable                              | Config file key                  | Default   | Value    | Example                    | Description                                                                        |
|-------------------------------------------|----------------------------------|-----------|----------|----------------------------|------------------------------------------------------------------------------------|
| * `S3PROXY_S3__URI`                       | `s3.uri`                         | unset     | HTTP URL | `http://localhost`         | URI of the S3 Object Storage                                                       |
| * `S3PROXY_S3__ACCESS_KEY`                | `s3.acess_key`                   | unset     | String   | `ZR7U56KMK20VW`            | Access key for the S3 that owns the buckets                                        |
| * `S3PROXY_S3__SECRET_KEY`                | `s3.secret_key`                  | unset     | String   | `9KRUU41EGSCB3H9ODECNHW`   | Secret key for the S3 that owns the buckets                                        |
| * `S3PROXY_S3__USERNAME`                  | `s3.username`                    | unset     | String   | `bucket-manager`           | ID of the user in ceph who owns all the buckets. Owner of `S3PROXY_S3__ACCESS_KEY` |
| `S3PROXY_S3__DEFAULT_BUCKET_SIZE_LIMIT`   | `s3.default_bucket_size_limit`   | `400 GiB` | ByteSize | `10 KB`, `10 KiB`, `10 MB` | Size limit of a new Bucket. Between `1 KiB` and `4.3 TB`                           |
| `S3PROXY_S3__DEFAULT_BUCKET_OBJECT_LIMIT` | `s3.default_bucket_object_limit` | `40000`   | Integer  | `10000`                    | Maximum number of objects in a new bucket. Must be $<2^{32}$                       |

### Security

| Env variable                       | Config file key            | Default            | Value  | Example | Description                        |
|------------------------------------|----------------------------|--------------------|--------|---------|------------------------------------|
| `S3PROXY_JWT_TOKEN_EXPIRE_MINUTES` | `jwt_token_expire_minutes` | 8 days             | number | 11520   | Minutes till a JWT expires         |
| `S3PROXY_SECRET_KEY`               | `secret_key`               | randomly generated | string | `xxxx`  | Secret key to sign Session Cookies |

### Lifescience OIDC

| Env variable                                | Config file key                   | Default                                  | Value    | Example                                  | Description                                                                   |
|---------------------------------------------|-----------------------------------|------------------------------------------|----------|------------------------------------------|-------------------------------------------------------------------------------|
| * `S3PROXY_LIFESCIENCE_OIDC__CLIENT_ID`     | `lifescience_oidc.client_id`      | unset                                    | string   | `xxx`                                    | OIDC Client secret                                                            |
| * `S3PROXY_LIFESCIENCE_OIDC__CLIENT_SECRET` | `lifescience_oidc.client_secret`  | unset                                    | string   | `xxx`                                    | OIDC Client ID                                                                |
| `S3PROXY_LIFESCIENCE_OIDC__BASE_URI`        | `lifescience_oidc.base_uri`       | `https://login.aai.lifescience-ri.eu`    | HTTP URL | `https://login.aai.lifescience-ri.eu`    | Lifescience OIDC Base URI. Will be concatenated with `S3PROXY_META_INFO_PATH` |
| `S3PROXY_LIFESCIENCE_OIDC__META_INFO_PATH`  | `lifescience_oidc.meta_info_path` | `/oidc/.well-known/openid-configuration` | string   | `/oidc/.well-known/openid-configuration` | Path to the OIDC meta data file                                               |

### Monitoring

| Env variable                  | Config file key      | Default | Value   | Example     | Description                                                                                  |
|-------------------------------|----------------------|---------|---------|-------------|----------------------------------------------------------------------------------------------|
| `S3PROXY_OTLP__GRPC_ENDPOINT` | `otlp.grpc_endpoint` | unset   | String  | `localhost` | OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger. If unset, no traces are sent. |
| `S3PROXY_OTLP__SECURE`        | `otlp.secure`        | `false` | Boolean | `false`     | Connection type                                                                              |

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. See
the [License](LICENSE) file for more information.
