Following mostly the [cephadm install guide](https://docs.ceph.com/en/pacific/cephadm/install/) to
setup a ceph cluster within Openstack.

# Requirements

- Create 6 cloud instances (with additional ephemeral disk space) and adjust inventory file
- Setup cloud instances.
  `ansible-playbook  -i hosts  site.yml`
- Clean up ephemeral disk. **This will remove an existing partition!**
  `ansible all  -i hosts -b -m shell -a "parted -s /dev/vdb rm 1"`
- Reboot (for kernel updates)
  `ansible all  -i hosts -b -m shell -a "reboot"`

# Install cephadm from URL

```
curl --silent --remote-name --location https://github.com/ceph/ceph/raw/pacific/src/cephadm/cephadm
chmod +x cephadm
sudo ./cephadm add-repo --release pacific
sudo ./cephadm install
```

# Bootstrap

- Bootstrap first node, make sure *NOT* using root as ssh user
`sudo cephadm bootstrap --mon-ip 192.168.192.118 --ssh-user ubuntu`



# Add more hosts

```
sudo cephadm shell -- ceph orch host add ceph-2 192.168.192.102 --labels _admin
sudo cephadm shell -- ceph orch host add ceph-3 192.168.192.155 --labels _admin
sudo cephadm shell -- ceph orch host add ceph-4 192.168.192.15
sudo cephadm shell -- ceph orch host add ceph-5 192.168.192.96
sudo cephadm shell -- ceph orch host add ceph-6 192.168.192.111
```

# Add additional monitors

- Deploy 5 monitor nodes on random hosts
`sudo cephadm shell -- ceph config set mon public_network 192.168.192.0/24`

# Add storage

- Add ephemeral disks as OSD
`sudo cephadm shell -- ceph orch apply osd --all-available-devices`

# Add RGWs

- Add two rados gateways
`sudo cephadm shell -- ceph orch apply rgw s3`
- Start two listen processes on each rgw host
`sudo cephadm shell -- ceph orch host label add ceph-1 rgw`
`sudo cephadm shell -- ceph orch host label add ceph-2 rgw`
`sudo cephadm shell -- ceph orch apply rgw s3 '--placement=label:rgw count-per-host:2' --port=8000`

# Create a VPN tunnel
With [sshuttle](https://github.com/sshuttle/sshuttle) you can easily create a VPN connection from your
workstation/notebook to our cloud based ceph cluster. **sshuttle** can be installed using `pip`.

`$ sshuttle -r  jkrueger@129.70.51.109:30118  192.168.192.0/24`
