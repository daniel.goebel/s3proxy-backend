"""Inital revision

Revision ID: 26795846bea8
Revises:
Create Date: 2024-04-30 16:37:20.430654

"""

from typing import Sequence

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision: str = "26795846bea8"
down_revision: str | None = None
branch_labels: str | Sequence[str] | None = None
depends_on: str | Sequence[str] | None = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "user",
        sa.Column("uid", sa.BINARY(length=16), nullable=False),
        sa.Column("lifescience_id", sa.String(length=64), nullable=True),
        sa.Column("display_name", sa.String(length=256), nullable=False),
        sa.Column("email", sa.String(length=256), nullable=True),
        sa.Column("created_at", sa.Integer(), server_default=sa.text("(UNIX_TIMESTAMP())"), nullable=False),
        sa.Column("updated_at", sa.Integer(), server_default=sa.text("(UNIX_TIMESTAMP())"), nullable=False),
        sa.PrimaryKeyConstraint("uid"),
    )
    op.create_index(op.f("ix_user_lifescience_id"), "user", ["lifescience_id"], unique=True)
    op.create_index(op.f("ix_user_uid"), "user", ["uid"], unique=True)
    op.create_table(
        "bucket",
        sa.Column("name", sa.String(length=63), nullable=False),
        sa.Column("description", mysql.TEXT(), nullable=False),
        sa.Column("public", sa.Boolean(), server_default="0", nullable=False),
        sa.Column("owner_id", sa.BINARY(length=16), nullable=True),
        sa.Column("size_limit", mysql.INTEGER(unsigned=True), nullable=True),
        sa.Column("object_limit", mysql.INTEGER(unsigned=True), nullable=True),
        sa.Column("created_at", sa.Integer(), server_default=sa.text("(UNIX_TIMESTAMP())"), nullable=False),
        sa.Column("updated_at", sa.Integer(), server_default=sa.text("(UNIX_TIMESTAMP())"), nullable=False),
        sa.ForeignKeyConstraint(["owner_id"], ["user.uid"], ondelete="SET NULL"),
        sa.PrimaryKeyConstraint("name"),
    )
    op.create_index(op.f("ix_bucket_name"), "bucket", ["name"], unique=True)
    op.create_table(
        "bucketpermission",
        sa.Column("uid", sa.BINARY(length=16), nullable=False),
        sa.Column("bucket_name", sa.String(length=63), nullable=False),
        sa.Column("from", sa.Integer(), nullable=True),
        sa.Column("to", sa.Integer(), nullable=True),
        sa.Column("file_prefix", sa.String(length=512), nullable=True),
        sa.Column("permissions", mysql.ENUM("READ", "WRITE", "READWRITE"), nullable=False),
        sa.Column("created_at", sa.Integer(), server_default=sa.text("(UNIX_TIMESTAMP())"), nullable=False),
        sa.Column("updated_at", sa.Integer(), server_default=sa.text("(UNIX_TIMESTAMP())"), nullable=False),
        sa.ForeignKeyConstraint(["bucket_name"], ["bucket.name"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["uid"], ["user.uid"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("uid", "bucket_name"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("bucketpermission")
    op.drop_index(op.f("ix_bucket_name"), table_name="bucket")
    op.drop_table("bucket")
    op.drop_index(op.f("ix_user_uid"), table_name="user")
    op.drop_index(op.f("ix_user_lifescience_id"), table_name="user")
    op.drop_table("user")
    # ### end Alembic commands ###
