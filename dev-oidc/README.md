# Mock OIDC Provider

! This OIDC Provider is only recommended for development systems !

## Quickstart standalone

```shell
docker run --name clowm_oidc_dev \
  -p 8002:80 \
  -v $(pwd):/tmp/config:ro \
  -e ASPNETCORE_ENVIRONMENT=Development \
  -e ASPNET_SERVICES_OPTIONS_PATH=/tmp/config/service_options.json \
  -e CLIENTS_CONFIGURATION_PATH=/tmp/config/clients_config.json \
  -e IDENTITY_RESOURCES_PATH=/tmp/config/identity_resources.json \
  -e SERVER_OPTIONS_PATH=/tmp/config/server_options.json \
  -e USERS_CONFIGURATION_PATH=/tmp/config/users_config.json \
  ghcr.io/soluto/oidc-server-mock:0.8.6
```


## Add a user

In copy a entry the file `users_config.json` and fill the appropriate entries with new values, e.g.
```json
{
  "SubjectId": "6ca3e9f1c2ca9efcd8679b6befcerfc6ab879fre68b6cafre7867ca45ef1c78c@lifescience-ri.eu",
  "Username": "okenobi",
  "Password": "password",
  "Claims": [
    {
      "Type": "name",
      "Value": "Obi-Wan Kenobi",
      "ValueType": "string"
    },
    {
      "Type": "email",
      "Value": "okenobi@uni-bielefeld.de",
      "ValueType": "string"
    }
  ]
}
```

## Remove a user
Remove the entry in the file `users_config.json`.

## Configure client
Configure the client who uses this OIDC provider in the file `clients_config.json`.
There  you can change the Client-ID, Client-Secret and redirect URLs.
This is highly recommended.
