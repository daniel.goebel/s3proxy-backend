#!/usr/bin/env bash

set -x

ruff --version
ruff check app migrations
ruff format --diff app migrations

isort --version
isort -c app migrations


mypy --version
mypy app
