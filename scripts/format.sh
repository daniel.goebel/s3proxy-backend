#!/bin/sh -e
set -x

ruff format app migrations
ruff check --fix --show-fixes app migrations
isort app migrations
